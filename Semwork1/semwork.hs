data Term = Var String 
          | Lam String Term 
          | App Term Term
          deriving(Eq)

instance Show Term where
    show (Var x) = x
    show (App term1 term2) = "(" ++ (show term1) ++ " " ++ (show term2)++ ")"
    show (Lam v t) = "(\\" ++ v ++ "." ++ (show t) ++ ")" 


renameList :: [String] -> String -> Int -> String
renameList arr x n | elem (x ++ (show n)) arr = renameList arr x (n+1)
                       | otherwise = x ++ (show n)

				   
changeAllVars :: [String] -> Term -> Term
changeAllVars arr (Lam x term) | elem x arr = Lam x (changeAllVars (filter (\el -> el /= x) arr) term)
                               | otherwise = Lam x (changeAllVars arr term)
changeAllVars arr (App t1 t2) = App (changeAllVars arr t1) (changeAllVars arr t2)
changeAllVars arr (Var x) | elem x arr = Var (renameList arr x 0)
                          | otherwise = Var x
	
	

watchFreeVar :: [String] -> Term -> String -> Term -> Term
watchFreeVar arr (Var y) x z = if x == y then (changeAllVars arr z) else (Var y)
watchFreeVar arr (Lam y v) x z = if x == y then (Lam y v) else (Lam y (watchFreeVar (y:arr) v x z))
watchFreeVar arr (App term1 term2) x z = App (watchFreeVar arr term1 x z) (watchFreeVar arr term2 x z)


eval1' :: Term -> Term
eval1' (App (Var x) t) = (App (Var x)(eval1' t))
eval1' (App (Lam x y) v) = watchFreeVar [] y x v
eval1' (App (App term1 term2) term3) = App (eval1' (App term1 term2)) term3
eval1' term = term


eval :: Term -> Term
eval (App (Var x) t) = (App (Var x) (eval t)) 
eval (App (Lam x y) v) = eval (watchFreeVar [] y x v)
eval (App (App term1 term2) term3) = eval (App (eval (App term1 term2)) term3)
eval term = term

--((\x.\y.x) z) ((\x. (x x))(\x. (x x)))
ex1 = App (App (Lam "x" (Lam "y" (Var "x"))) (Var "z")) (App (Lam "x" (App (Var "x") (Var "x"))) (Lam "x" (App (Var "x") (Var "x"))))
--((\x.x)((\x.x)(\z.(\x.x) z)))
ex2 = App (Lam "x"(Var "x")) (App (Lam "x"(Var "x")) (App (Lam "z" (Lam "x" (Var "x"))) (Var "z"))) 
--(\x.\y y) y
ex3 = App (Lam "x" (Lam "y" (Var "y"))) (Var "y") 
--(\x.\y x) z
ex4 = App (Lam "x" (Lam "y" (Var "x"))) (Var "z") 
--(\x.\y x) (\z.z)
ex5 = App (Lam "x" (Lam "y" (Var "x"))) (Lam "z" (Var "z")) 
--((\x.x x)(\x.x x)) not has normal form
ex7 = App (App (Lam "x"(Var "x")) (Var "x")) (App (Lam "x"(Var "x")) (Var "x"))