-- Тесты чуть позже

module HW2
        ( Contact (..)
        , isKnown
        , Term (..)
        , eval
        , simplify
        ) where
 
 data Contact = On
              | Off
              | Unknown
              deriving (Show, Eq)
 
 isKnown :: Contact -> Bool
 isKnown Unknown = False
 isKnown _ = True
 
 data Term = Mult Term Term      -- умножение
           | Add Term Term       -- сложение
           | Sub Term Term       -- вычитание
           | Const Int           -- константа

           deriving (Show, Eq)
 eval :: Term -> Int
 eval (Const x) = x
 eval (Mult x y) = (eval x) * (eval y)
 eval (Add x y) = (eval x) + (eval y)
 eval (Sub x y) = (eval x) - (eval y)

 
 -- Раскрыть скобки
 -- Mult (Add (Const 1) (Const 2)) (Const 3) ->
 -- Add (Mult (Const 1) (Const 3)) (Mult (Const 2) (Const 3))
 -- (1+2)*3 -> 1*3+2*3
 simplify :: Term -> Term
 simplify (Const x) = Const x
 simplify (Mult (Add x y) z) = Add (Mult (simplify x) (simplify z)) (Mult (simplify y) (simplify z))
 simplify (Mult x (Add y z)) = Add (Mult (simplify x) (simplify y)) (Mult (simplify x) (simplify z))
 simplify (Mult (Sub x y) z) = Sub (Mult (simplify x) (simplify y)) (Mult (simplify x) (simplify z))
 simplify (Mult x (Sub y z)) = Sub (Mult (simplify x) (simplify y)) (Mult (simplify x) (simplify z))
 simplify (Mult x y) = Mult x y
 simplify (Sub x y) = Sub x y
 simplify (Add x y) = Add x y
 